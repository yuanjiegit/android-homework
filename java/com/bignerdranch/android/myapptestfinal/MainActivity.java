package com.bignerdranch.android.myapptestfinal;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;

import com.bignerdranch.android.myapptestfinal.fragment.MyFragmentFour;
import com.bignerdranch.android.myapptestfinal.fragment.MyFragmentOne;
import com.bignerdranch.android.myapptestfinal.fragment.MyFragmentThree;
import com.bignerdranch.android.myapptestfinal.fragment.MyFragmentTwo;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {
    //UI Object
    private RadioButton rbtnChannel, rbtnMessage, rbtnMy, rbtnMore;
    private RadioGroup rgroupTabMenu;

    //Fragment Object
    private MyFragmentOne fg1;
    private MyFragmentTwo fg2;
    private MyFragmentThree fg3;
    private MyFragmentFour fg4;
    private FragmentManager fManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fManager = getFragmentManager();
        bindViews();

        rbtnChannel.setChecked(true);
    }

    //UI组件初始化与事件绑定
    private void bindViews() {
        rgroupTabMenu = findViewById(R.id.main_rgroupTabMenu);

        rbtnChannel = findViewById(R.id.main_rbtnChannel);
        rbtnMessage = findViewById(R.id.main_rbtnMessage);
        rbtnMy = findViewById(R.id.main_rbtnMy);
        rbtnMore = findViewById(R.id.main_rbtnMore);


        rgroupTabMenu.setOnCheckedChangeListener(this);
    }

    //隐藏所有Fragment
    private void hideAllFragment(FragmentTransaction fragmentTransaction){
        if(fg1 != null)fragmentTransaction.hide(fg1);
        if(fg2 != null)fragmentTransaction.hide(fg2);
        if(fg3 != null)fragmentTransaction.hide(fg3);
        if(fg4 != null)fragmentTransaction.hide(fg4);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        FragmentTransaction fTransaction = fManager.beginTransaction();
        hideAllFragment(fTransaction);
        switch (checkedId){
            case R.id.main_rbtnChannel:
                if(fg1 == null){
                    fg1 = new MyFragmentOne();
                    fTransaction.add(R.id.ly_content,fg1);//动态添加一个fragment
                }else{
                    fTransaction.show(fg1);
                }
                break;
            case R.id.main_rbtnMessage:
                if(fg2 == null){
                    fg2 = new MyFragmentTwo();
                    fTransaction.add(R.id.ly_content,fg2);
                }else{
                    fTransaction.show(fg2);
                }
                break;
            case R.id.main_rbtnMy:
                if(fg3 == null){
                    fg3 = new MyFragmentThree();
                    fTransaction.add(R.id.ly_content,fg3);
                }else{
                    fTransaction.show(fg3);
                }
                break;
            case R.id.main_rbtnMore:
                if(fg4 == null){
                    fg4 = new MyFragmentFour();
                    fTransaction.add(R.id.ly_content,fg4);
                }else{
                    fTransaction.show(fg4);
                }
                break;
        }
        fTransaction.commit();//提交事务
    }


}




