package com.bignerdranch.android.myapptestfinal;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Map;

public class TwoActivity extends AppCompatActivity {
    private Button button_dl;
    private Button button2;
    public static EditText editText_username;
    public static EditText editText_password;
    private TextView show;
    private TextView regiser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);



        editText_username=(EditText) findViewById(R.id.et1);
        editText_password=(EditText)findViewById(R.id.et2);

        final Map<String, String> userInfo = FileSave.getUserInfo(this);
        if (userInfo != null) {

            editText_username.setText(userInfo.get("account"));   //将获取的账号显示到界面上
            editText_password.setText(userInfo.get("password")); //将获取的密码显示到界面上
        }

        show=(TextView)findViewById(R.id.tv1);

        button_dl=(Button)findViewById(R.id.bt_dl);

        button_dl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username=editText_username.getText().toString().trim();
                String password=editText_password.getText().toString().trim();
                if (TextUtils.isEmpty(username)&&TextUtils.isEmpty(password)){
                    Toast.makeText(TwoActivity.this,
                            "请输入账号和密码哦",Toast.LENGTH_SHORT).show();
                }else if (TextUtils.isEmpty(username)){
                    Toast.makeText(TwoActivity.this,
                            "请输入账号",Toast.LENGTH_SHORT).show();
                }else if (TextUtils.isEmpty(password)){
                    Toast.makeText(TwoActivity.this,
                            "你没输入密码",Toast.LENGTH_SHORT).show();
                }else if(username.equals(userInfo.get("account")) && password.equals(userInfo.get("password"))){
                    show.setText("用户名为："+username+"密码为："+password);
                    Toast.makeText(TwoActivity.this,
                            "登录成功",Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(TwoActivity.this,MainActivity.class);
                    startActivity(intent);

                }
                else {
                    Toast.makeText(TwoActivity.this,
                            "账号或密码错误！",Toast.LENGTH_SHORT).show();

                }

                //登录验证


            }
        });
        button2=(Button)findViewById(R.id.bt2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TwoActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
        regiser=(TextView)findViewById(R.id.tv_register);
        regiser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TwoActivity.this,RegisertActivity.class);
                startActivity(intent);
            }
        });



    }

    @Override
    public void onBackPressed() {
        AlertDialog dialog;
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("这是一个对话框");
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage("真的要退出吗：");

        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                TwoActivity.this.finish();
            }
        });
        builder.setNegativeButton("放弃", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog=builder.create();
        dialog.show();

    }
}