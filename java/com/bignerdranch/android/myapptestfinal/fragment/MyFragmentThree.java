package com.bignerdranch.android.myapptestfinal.fragment;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.bignerdranch.android.myapptestfinal.R;
import com.bignerdranch.android.myapptestfinal.TwoActivity;

public class MyFragmentThree extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fg_content_three, container, false);
        return view;
    }
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        变量初始化
        Button bt_1=getActivity().findViewById(R.id.bts_one);

//        测试按钮点击事件
        bt_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), TwoActivity.class);
                startActivity(intent);
            }
        });
    }
}
