package com.bignerdranch.android.myapptestfinal;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Map;

public class RegisertActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et_account, et_password1, et_password2;
    Button zc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regisert);

        et_account = (EditText) findViewById(R.id.re_et1);
        et_password1 = (EditText) findViewById(R.id.re_et2);
        et_password2 = (EditText) findViewById(R.id.re_et3);
        zc = (Button) findViewById(R.id.btn_zc);
        zc.setOnClickListener(this);

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_zc:
                String account = et_account.getText().toString().trim();
                String password1 = et_password1.getText().toString().trim();
                String password2 = et_password2.getText().toString().trim();

                if (TextUtils.isEmpty(account)) {
                    Toast.makeText(this, "请输账号", Toast.LENGTH_SHORT).show();

                } else if (TextUtils.isEmpty(password1) || TextUtils.isEmpty(password2)) {
                    Toast.makeText(this, "请输密码", Toast.LENGTH_SHORT).show();

//                }else if (password1.equals(password2)) {
//                    Toast.makeText(this, "注册成功！", Toast.LENGTH_SHORT).show();
//                    break;
                } else if (password1.equals(password2)) {
                    //将账号密码存储到文件类中
                    boolean isSaveSuccess = FileSave.saveUserInfo(this, account, password1);

                    if (isSaveSuccess) {
                        Toast.makeText(this, "注册成功", Toast.LENGTH_SHORT).show();

                        Map<String, String> userInfo = FileSave.getUserInfo(this);
                        if (userInfo != null) {
                            //注册成功即时将注册成功的账号密码显示到登录信息框
                            TwoActivity.editText_username.setText(userInfo.get("account"));   //将获取的账号显示到界面上
                            TwoActivity.editText_password.setText(userInfo.get("password")); //将获取的密码显示到界面上
                            finish();
                        }

                    } else {
                        Toast.makeText(this, "注册失败", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(this, "两次密码不一致！", Toast.LENGTH_SHORT).show();

                }
        }

    }
}